package com.jkroub.jira6611.beans.postprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
    public MyBeanPostProcessor() {
        System.out.println(String.format("CONSTRUCTING: %s", getClass().getName()));
    }
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName)
            throws BeansException {
        System.out.println(String.format("%s:postProcessBeforeInitialization: %s", getClass().getSimpleName(), beanName));
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName)
            throws BeansException {
        System.out.println(String.format("%s:postProcessAfterInitialization: %s", getClass().getSimpleName(), beanName));
        return bean;
    }

}
