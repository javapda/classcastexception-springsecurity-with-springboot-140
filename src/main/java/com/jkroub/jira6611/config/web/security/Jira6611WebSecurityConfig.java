package com.jkroub.jira6611.config.web.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.jkroub.jira6611.beans.postprocessor.MyBeanPostProcessor;
import com.jkroub.jira6611.web.security.MyAuthenticationProvider;
import com.jkroub.jira6611.web.security.MyPreAuthenticatedProcessingFilter;

@Configuration
@ComponentScan(basePackageClasses = { MyBeanPostProcessor.class, MyAuthenticationProvider.class, MyPreAuthenticatedProcessingFilter.class })
@EnableWebSecurity
public class Jira6611WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private static final Logger log = LogManager.getLogger(Jira6611WebSecurityConfig.class);
    public Jira6611WebSecurityConfig() {
        if(log.isInfoEnabled()) {
            log.info(String.format("CONSTRUCTING: %s",getClass().getSimpleName()));
        }
    }
}
