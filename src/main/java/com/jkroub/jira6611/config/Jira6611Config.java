package com.jkroub.jira6611.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.jkroub.jira6611.config.web.security.Jira6611WebSecurityConfig;

@Configuration
@Import(value = { Jira6611WebSecurityConfig.class })
public class Jira6611Config {

}
