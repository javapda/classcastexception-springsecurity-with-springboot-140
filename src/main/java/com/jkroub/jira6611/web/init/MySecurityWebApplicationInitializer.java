package com.jkroub.jira6611.web.init;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

import com.jkroub.jira6611.config.Jira6611Config;

@Order(value=Ordered.HIGHEST_PRECEDENCE)
public class MySecurityWebApplicationInitializer 
	extends AbstractSecurityWebApplicationInitializer {

    public MySecurityWebApplicationInitializer() {
        super(Jira6611Config.class);
    }
    
    @PostConstruct
    public void postConstruct() {
    	System.out.println(String.format("POST-CONSTRUCT: %s", getClass().getSimpleName()));
    }
    
    @Override
	protected void afterSpringSecurityFilterChain(ServletContext servletContext) {
    	System.out.println(String.format("afterSpringSecurityFilterChain: %s", getClass().getSimpleName()));
	}

    
}
