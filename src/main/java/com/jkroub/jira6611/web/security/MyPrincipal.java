package com.jkroub.jira6611.web.security;

import java.io.Serializable;

import org.springframework.security.core.userdetails.User;

public class MyPrincipal implements Serializable {
    private static final long serialVersionUID = 1L;

    private String username, password;
    protected MyPrincipal(User user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
    }   

    public static MyPrincipal create(User user) {
        return new MyPrincipal(user);
    }
    
    public String toString() {
        return this.username;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }
}
