package com.jkroub.jira6611.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.stereotype.Component;


@Component(MyPreAuthenticationProvider.NAME)
public class MyPreAuthenticationProvider extends
		PreAuthenticatedAuthenticationProvider {
    
    public static final String NAME = "GsPreAuthenticationProvider";

	
	@Autowired
	public MyPreAuthenticationProvider(MyPreAuthenticationUserDetailsService userDetailsService2) {
		super();
		setPreAuthenticatedUserDetailsService(userDetailsService2);
	}

	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		System.out.println("authentication.getPrincipal():  "+authentication.getPrincipal());
		User user = makeUser(authentication.getPrincipal().toString());
		if(user!=null) {
			return MyAuthentication.create(user);
		}
		return super.authenticate(authentication);
	}

    private User makeUser(String username) {
        return new User(username,"password",MySecurityUtil.authorities());
    }
	
}
