package com.jkroub.jira6611.web.security;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.PriorityOrdered;
import org.springframework.security.access.annotation.Jsr250MethodSecurityMetadataSource;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.stereotype.Component;

/**
 * http://docs.spring.io/spring-security/site/migrate/current/3-to-4/html5/
 * migrate-3-to-4-xml.html#m3to4-role-prefixing-disable One can disable
 * automatic ROLE_ prefixing using a BeanPostProcessor
 * 
 * @author jkroub
 *
 */
@Component
public class MyDisableRolesPrefixPostProcessor implements BeanPostProcessor, PriorityOrdered {
    
    public MyDisableRolesPrefixPostProcessor() {
        System.out.println(String.format("CONSTRUCTING: %s", getClass().getSimpleName() ));
    }

	@Override
	public int getOrder() {
		return PriorityOrdered.HIGHEST_PRECEDENCE;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
	    System.out.println(String.format("%s:postProcessAfterInitialization: %s", getClass().getSimpleName(), beanName ));
		// remove this if you are not using JSR-250
		if (bean instanceof Jsr250MethodSecurityMetadataSource) {
			((Jsr250MethodSecurityMetadataSource) bean).setDefaultRolePrefix(null);
		}

		if (bean instanceof DefaultMethodSecurityExpressionHandler) {
			((DefaultMethodSecurityExpressionHandler) bean).setDefaultRolePrefix(null);
		}
		if (bean instanceof DefaultWebSecurityExpressionHandler) {
			((DefaultWebSecurityExpressionHandler) bean).setDefaultRolePrefix(null);
		}
		if (bean instanceof SecurityContextHolderAwareRequestFilter) {
			((SecurityContextHolderAwareRequestFilter) bean).setRolePrefix("");
		}
		return bean;
	}

}
