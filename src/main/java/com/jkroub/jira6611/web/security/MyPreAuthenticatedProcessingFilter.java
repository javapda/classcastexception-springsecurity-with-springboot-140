package com.jkroub.jira6611.web.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;
import org.springframework.stereotype.Component;

import com.jkroub.jira6611.web.security.service.MyAuthenticationService;
import com.jkroub.jira6611.web.security.service.MyPreAuthenticationService;

@Component
public class MyPreAuthenticatedProcessingFilter extends AbstractPreAuthenticatedProcessingFilter {
    private AuthenticationManager am;
    private MyPreAuthenticationService myPreAuthenticationService;
    private MyAuthenticationService myAuthenticationService;

    @Autowired
    public MyPreAuthenticatedProcessingFilter(
            @Qualifier(MyAuthenticationManager.NAME) AuthenticationManager am,
            MyPreAuthenticationService preAuthenticationService,
            MyAuthenticationService gsAuthenticationService) {
        System.out.println(String.format("CONSTRUCTING: %s", getClass().getSimpleName()));
        this.am = am;
        this.myPreAuthenticationService = preAuthenticationService;
        this.myAuthenticationService = gsAuthenticationService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        super.doFilter(request, response, chain);
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void afterPropertiesSet() {
        System.out.println(String.format("Setting Authentication Manager in %s", getClass().getSimpleName()));
        setAuthenticationManager(am);
        super.afterPropertiesSet();
    }


}
