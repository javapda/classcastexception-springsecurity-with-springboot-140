package com.jkroub.jira6611.web.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.userdetails.User;

public class MyUserDetails extends User {
    private static final long serialVersionUID = -7525759893504225957L;
    protected static Logger log = LogManager.getLogger(MyUserDetails.class);
    public MyUserDetails(User user) {
        super(user.getUsername(),user.getPassword(),MySecurityUtil.authorities());
        System.out.println(String.format("CONSTRUCTING: %s", getClass().getSimpleName()));
        
    }
    public static MyUserDetails create(User findUser) {
        return new MyUserDetails(findUser);
    }

}
