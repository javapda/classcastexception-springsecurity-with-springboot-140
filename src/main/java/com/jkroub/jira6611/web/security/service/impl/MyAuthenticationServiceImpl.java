package com.jkroub.jira6611.web.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.jkroub.jira6611.web.security.MyAuthentication;
import com.jkroub.jira6611.web.security.MyAuthenticationManager;
import com.jkroub.jira6611.web.security.MySecurityUtil;
import com.jkroub.jira6611.web.security.service.MyAuthenticationService;

@Service(value=MyAuthenticationServiceImpl.NAME)
public class MyAuthenticationServiceImpl implements MyAuthenticationService {
    public static final String NAME = "myAuthenticationService";
    private AuthenticationManager authenticationManager;
//	private UserService userService;
	
	@Autowired
	public MyAuthenticationServiceImpl(@Qualifier(MyAuthenticationManager.NAME) AuthenticationManager authenticationManager) {
//	    
//	},
//	        UserService userService) {
	    this.authenticationManager = authenticationManager;
//	    this.userService = userService;
	}
	
	@Override
	public boolean authenticate(String principal, String credentials) {
		Authentication request = new UsernamePasswordAuthenticationToken(principal,credentials);
		Authentication result = authenticationManager.authenticate(request);
		return result.isAuthenticated();
	}

	@Override
	public MyAuthentication getAuthenticationForUser(Long userId) {
		return MyAuthentication.create(MySecurityUtil.determinUser());
	}

}
 