package com.jkroub.jira6611.web.security;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedGrantedAuthoritiesUserDetailsService;
import org.springframework.stereotype.Service;


@Service(MyPreAuthenticationUserDetailsService.NAME)
public class MyPreAuthenticationUserDetailsService extends
		PreAuthenticatedGrantedAuthoritiesUserDetailsService {
    public static final String NAME = "myPreAuthenticationUserDetailsService";

	@Override
	protected UserDetails createUserDetails(Authentication token,
			Collection<? extends GrantedAuthority> authorities) {
		String username = (String) token.getPrincipal();
		System.out.println(String.format("username: %s",username));
		User user = new User(username, "password", MySecurityUtil.authorities());
		return MyUserDetails.create(user);
	}
	
}
