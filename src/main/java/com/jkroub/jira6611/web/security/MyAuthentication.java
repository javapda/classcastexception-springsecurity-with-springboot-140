package com.jkroub.jira6611.web.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;


public class MyAuthentication extends UsernamePasswordAuthenticationToken {
    private static final long serialVersionUID = 365530616936974512L;
    private MyUserDetails userDetails;
    private MyPrincipal principal;
    
    public MyAuthentication(User user) {
        super(user.getUsername(),user.getPassword(),MySecurityUtil.authorities());
        this.userDetails = MyUserDetails.create(user);
        this.principal = MyPrincipal.create(user);
    }   

    public static MyAuthentication create(User user) {
        return new MyAuthentication(user);
    }
    
    @Override
    public String getName() {
        return (principal == null) ? null : principal.getUsername();
    }

    @Override
    public Object getCredentials() {
        return userDetails.getPassword();
    }

    @Override
    public Object getDetails() {
        return this.userDetails;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

}
