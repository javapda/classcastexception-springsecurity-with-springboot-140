package com.jkroub.jira6611.web.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.stereotype.Component;
@Component(MyAuthenticationManager.NAME)
public class MyAuthenticationManager extends ProviderManager {
    public static final String NAME = "myAuthenticationManager";
    // public static final String NAME = "authenticationManager";

    @Autowired
    public MyAuthenticationManager(
            @Qualifier(MyAuthenticationProvider.NAME) AuthenticationProvider myAuthenticationProvider,
            @Qualifier(MyPreAuthenticationProvider.NAME) AuthenticationProvider myPreAuthenticationProvider) {
        super(Arrays.asList(new AuthenticationProvider[] { 
                myPreAuthenticationProvider,
                myAuthenticationProvider 
                }));
    }   

}
