package com.jkroub.jira6611.web.security.service;

import com.jkroub.jira6611.web.security.MyAuthentication;

public interface MyAuthenticationService {
	public boolean authenticate(String userName, String password);
	MyAuthentication getAuthenticationForUser(Long userId);
}
