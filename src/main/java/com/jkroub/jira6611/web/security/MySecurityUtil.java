package com.jkroub.jira6611.web.security;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

//import com.gs.core.domain.authorization.AuthorizationGroupRole;
//import com.gs.core.domain.parties.User;

public class MySecurityUtil {
//    public static List<? extends GrantedAuthority> convertToAuthorities(User user) {
//        List<SimpleGrantedAuthority> s = new ArrayList<SimpleGrantedAuthority>();
////      if(user.getUserType()!=null) {
////          s.add(new SimpleGrantedAuthority("ROLE_"+user.getUserType()));
////      }
//        for ( AuthorizationGroupRole  groupRole : user.getGroupRoles() ) {
//            s.add(new SimpleGrantedAuthority(groupRole.getGroupName()));
//        }
//        return s;
//    }
    public static Collection<? extends GrantedAuthority> authorities() {
        return Stream.of(new SimpleGrantedAuthority("ANY")).collect(Collectors.toList());
    }

    public static User determinUser() {
        // TODO Auto-generated method stub
        return new User("someuser", "password", authorities());
    }

}
