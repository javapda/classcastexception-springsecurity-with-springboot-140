package com.jkroub.jira6611.web.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;


@Component(MyAuthenticationProvider.NAME)
public class MyAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    public static final String NAME = "myAuthenticationProvider";
    protected static Logger log = LogManager.getLogger(MyAuthenticationProvider.class);
    public MyAuthenticationProvider() {
        System.out.println(String.format("CONSTRUCTING: ",getClass().getSimpleName()));
    }
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        super.authenticate(authentication);
        String username = null;
        String password = null;
        if (authentication instanceof UsernamePasswordAuthenticationToken) {
            UsernamePasswordAuthenticationToken upat = (UsernamePasswordAuthenticationToken) authentication;
            username = upat.getPrincipal().toString();
            password = (String) upat.getCredentials().toString();
        } else {
            MyAuthentication auth = (MyAuthentication) authentication;
            username = auth.getPrincipal().toString();
        }
        
        Authentication auth = MyAuthentication.create(fakeUser(username));
        SecurityContextHolder.getContext().setAuthentication(auth);
        return auth;
    }
    @Override
    protected void additionalAuthenticationChecks(UserDetails arg0,
            UsernamePasswordAuthenticationToken arg1) throws AuthenticationException {
        
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
            throws AuthenticationException {
        User user = fakeUser(username);
        if (user == null)
            throw new UsernameNotFoundException("User: " + username + " not found.");
        return new MyUserDetails(user);

    }
    
    private User fakeUser(String username) {
        return new User(username,"password",MySecurityUtil.authorities());
    }

}
