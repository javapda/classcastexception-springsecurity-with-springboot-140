package com.jkroub.jira6611.unittest;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.jkroub.jira6611.config.Jira6611Config;

@Configuration
@Import(value={Jira6611Config.class})
public class Jira6611TestConfig {

}
