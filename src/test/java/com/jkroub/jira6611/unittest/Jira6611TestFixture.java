package com.jkroub.jira6611.unittest;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Jira6611TestConfig.class })
public class Jira6611TestFixture {
    @Autowired private ApplicationContext applicationContext;
    @Test
    public void test() {
        // fail("Not yet implemented");
        showBeans();
    }
    protected void showBeans() {
        Arrays.asList(applicationContext.getBeanDefinitionNames()).stream().sorted().forEach(System.out::println);
    }

}
